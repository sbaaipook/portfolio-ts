import React from "react";
import "./index.css"

const About:React.FC=()=>{
  return(
    <section className="container-about">
      <h1 className="about-title">About Me</h1>
      <div className="about-container">
        <img src="/assets/images/about-me-photo.jpg" alt="abdosb" className="img-about" />
        <div className="about-body">
          <h1 className="my-name">Abdessadek Sbaai</h1>
          <p className="about-me">
            Lorem ipsum dolor sit amet, officia excepteur ex fugiat reprehenderit enim
            labore culpa sint ad nisi Lorem pariatur mollit ex esse exercitation amet.
            Nisi anim cupidatat excepteur officia.
            Reprehenderit nostrud nostrud ipsum Lorem est aliquip amet voluptate
            voluptate dolor minim nulla est proident.
            Nostrud officia pariatur ut officia.
            Sit irure elit esse ea nulla sunt ex occaecat 
            reprehenderit commodo officia dolor 
            Lorem duis laboris cupidatat officia voluptate.
            Culpa proident adipisicing id nulla nisi laboris ex in Lorem sunt duis officia eiusmod.
            Aliqua reprehenderit commodo ex non excepteur duis sunt velit enim.
            Voluptate laboris sint cupidatat ullamco ut ea consectetur et est culpa et culpa duis.
          </p>
        </div>
      </div>
    </section>
  )
}

export default About;
