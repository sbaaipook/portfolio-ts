import React from "react"
import HeadSection from "../../components/head-section";
import Projects from "../../components/projects";
import Services from "../../components/services"
import Main from "../../containers/main";
import "./index.css"


const Home:React.FC =()=>{
  return (
    <div className="home">
      <HeadSection/>
      <Main>
        <Projects />
        <Services />
      </Main>
    </div>
  )
}
export default Home;
