export interface Projs{
  id:number;
  imgProject:string;
  titleProject:string;
  description:string;
  type:string;
  state:string;
  utilities:string[];
}
