import React from 'react';
import { Routes, Route } from "react-router-dom"
import About from '../../pages/about';
import Blog from '../../pages/blog';
import Contact from '../../pages/contact';
import Home from '../../pages/home';
import Projects from '../../pages/projects';
import Layout from '../layout';



const App:React.FC =()=> {

  return (
    <Layout>
        <Routes>
          <Route path='/' element={<Home/>}/>
          <Route path='/about' element={<About/>} />
          <Route path='/contact' element={<Contact/>} />
          <Route path='/blog' element={<Blog/>} />
          <Route path='/projects' element={<Projects/>} />
        </Routes>
    </Layout>
  );
}

export default App;

