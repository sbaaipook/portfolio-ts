import React from "react";
import './index.css'

type Props = {
  children:React.ReactNode
}

const Main:React.FC<Props> =({children})=>{
  return(
    <main className="home-main">
      {children}
    </main>
  )
}
export default Main;
