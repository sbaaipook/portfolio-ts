import React from "react"
import { BrowserRouter as Router } from "react-router-dom"
import Footer from "../../commons/footer";
import Navbar from "../../commons/navbar";
import "./index.css"

type Props = {
  children: React.ReactNode;
}
const Layout:React.FC<Props> =({children})=>{
  return(
    <div className="layout">
      <Router>
        <Navbar />
        { children }
        <Footer />
      </Router>
    </div>
  )
}
export default Layout;
