import React from "react";
import { Link } from 'react-router-dom';
import { BsLinkedin, BsGithub, BsTelegram } from 'react-icons/bs'
import { AiFillTwitterCircle } from 'react-icons/ai'
import "./index.css"
import  { TypeAnimation } from "react-type-animation";


const HeadSection:React.FC =()=>{
  return(
    <section className="container">
      <div className="effct-profile-img">
        <div className="effct-profile-img-1"></div>
        <div className="effct-profile-img-3"></div>
        <div className="effct-profile-img-2">
          <img src="assets/images/myicon.png" alt="myicon" style={{width:"100%",height:'100%'}}/>
        </div> 
      </div>
      <div className="info-profile">
        <h1 className="hello">Hello world! my name is <span className="light">Abdessadek</span></h1>
        <h2 className="iam"> 
          <TypeAnimation
            sequence={[
            "I'm FRONT-END", 
            2000,
            "I'm BACK-END",
            2000, 
            ()=>{}
          ]}
          wrapper="span"
          cursor={true}
          repeat={Infinity}
        />
        <span className="light">Devloper</span></h2>
        <p className="text-info">
          Lorem ipsum dolor sit amet consectetur adipisicing.
          Aspernatur, quibusdam.

        </p>
        <Link to='/about' className="more">More ..</Link>
      </div>
      <div className="social-media">
        <a href={'https://www.linkedin.com/in/abdessadek-sbaai-554b1b259'}  target={"_blank"} rel="noreferrer" className="social"><BsLinkedin className='spadding so'/></a>
        <a href={'https://github.com/sbaaipook'} target={'_blank'} rel="noreferrer" className="social"><BsGithub className='so spadding'/></a>
        <a href={'https://t.me/Abdo3670'} target={'_blank'} rel="noreferrer" className="social"><BsTelegram className='so spadding'/></a>
        <a href={'https://twitter.com/sbaaipook?s=09'} target={'_blank'} rel="noreferrer" className="social"><AiFillTwitterCircle className='so'/></a>
      </div>
    </section>
  )
}
export default HeadSection;
