import React from 'react';
import { Link } from 'react-router-dom';
import { Projs } from '../../helpers/types/type-project';
import './index.css'


const SingleProject:React.FC<Projs> =(props)=>{
  

  return(
    <div className={'single-pro'}>
      <img src={props.imgProject} alt={props.titleProject} />
      <div className='single-pro-body'>
        <h1>{props.titleProject}</h1>
        <p className='description'>{props.description}</p>
        <span className='utilities'>{props.utilities.map(util=>(
          `${util} `
        ))}</span>
        <span className={props.state.toLowerCase()==="done"?"done":"progress"}>{props.state}</span>
      </div>   
      <Link to={`/`} className="more-ditails">More ...</Link>
    </div>
  )
}
export default SingleProject; 
