import React, { useEffect, useState } from 'react';
import './index.css';
import SingleService, { Porps } from '../single-service'
import axios from 'axios';



const Services:React.FC =()=>{
  
  const [ services,setServices ] = useState<Porps[]>([])

  useEffect(()=>{
    axios.get("/assets/data/services.json")
    .then(response=>setServices(response.data))
  },[])

  return(
    <div className="services">
      <h1 className="services-title">
        Services
      </h1>
      <div className="single-service-container">
        {
          services ? services.map(service=>(
            <SingleService {...service} />
          )): <></>
        }
      </div> 
    </div>
  )
}
export default Services;
