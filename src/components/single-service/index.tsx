import React from "react";
import "./index.css";

export interface Porps {
  id?:number;
  src:string;
  title:string;
  description:string;
  tools:string[]
}

const SingleService:React.FC<Porps>=({title,src,description,tools})=>{
  return(
    <div className="single-service">
      <img src={src} alt={title} />
      <h2 className="service-title">{title}</h2>
      <p className="service-description">
        {description}
      </p>
      <ul className="tools">
        {
          tools && tools.map(tool=><li>{tool}</li>)
        }
      </ul>
    </div>
  )
}
export default SingleService;
