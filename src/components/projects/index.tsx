import React, { useEffect, useState } from "react";
import SingleProject from "../single-project";
import './index.css'
import axios from "axios";
import { Projs } from "../../helpers/types/type-project";
import { Link } from "react-router-dom";
import { BsArrowRight } from "react-icons/bs";



const Projects:React.FC =()=>{

  const [projects, setProjects] = useState<Projs[]>();

  useEffect(()=>{
    axios.get('/assets/data/projects.json')
    .then(response=>{
        setProjects(response.data)
      })
  },[])

  return(
   <div className="projects">
      <div className="circle"></div>
      <div className="circle"></div>
      <div className="circle"></div>
      <div className="circle"></div>
      <h1 className="title">
        Projects
      </h1>
      <div className="projects-container">
        {
          projects ? projects.map(project=>(
            <SingleProject
              key={project.id}
              {...project}
            />
          )):<></>
        }
      </div>
      <Link to={"/projects"} className="all-projects">
        All projects <BsArrowRight />
      </Link>
    </div> 
  )
}

export default Projects;
