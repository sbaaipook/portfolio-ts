import React, { useState } from "react";
import { Link } from "react-router-dom";
import './index.css'

const Navbar:React.FC =()=>{
  const [activeBlog, setActiveBlog] = useState<string>('');
  const [activeHome, setActiveHome] = useState<string>('active-nav');
  const [activeProjects, setActiveProjects] = useState<string>('');
  const [activeAbout, setActiveAbout] = useState<string>('');
  return(
    <div className="header">
      <header>
        <Link to="/" className="logo"><span className="part-of-logo">Abde</span>ssadek</Link>
        <ul className="navbar">
          <a href={'/assets/others/abdosb.pdf'} className="cv" download>
            <li>Download CV</li>
            <span className="effect"></span>
          </a>
          <Link to={'/contact'} className="item active">
            <li>Contact</li>
          </Link>
        </ul>
      </header>
      <ul className="nav">
        <Link 
          to={'/'}
          onClick={ ()=>{setActiveHome('active-nav');setActiveBlog('');setActiveProjects('');setActiveAbout('')} }
          className={`nav-item ${activeHome}`}>

          <li>Home</li>

        </Link>
        <Link 
          to={'/projects'}
          onClick={()=>{setActiveHome('');setActiveBlog('');setActiveProjects('active-nav');setActiveAbout('')}}
          className={`nav-item ${activeProjects}`}>
          
          <li>Projects</li>
        
        </Link>
        <Link 
          onClick={ ()=>{setActiveHome('');setActiveBlog('active-nav');setActiveProjects('');setActiveAbout('')} }
          to={'/blog'} 
          className={`nav-item ${activeBlog}`}>
          <li>Blog</li>
        </Link>
        <Link 
          onClick={ ()=>{setActiveHome('');setActiveBlog('');setActiveProjects('');setActiveAbout('active-nav')} }
          to={'/about'} 
          className={`nav-item ${activeAbout}`}>
          <li>About</li>
        </Link>
      </ul>
    </div>
  )
}

export default Navbar;
