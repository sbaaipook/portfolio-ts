import React from 'react'
import { AiFillGitlab, AiFillInstagram } from 'react-icons/ai';
import { FaWhatsappSquare } from 'react-icons/fa'
import './index.css'

const Footer:React.FC=()=>{
  return(
    <footer>
      
      <div className="copyright">
        <span className='copy' >© 2022 Abdessadek sbaai |</span>
        <span className='contact'>
          <a href={'/'} target={'_blank'} rel="noreferrer"><AiFillGitlab/></a>
          <a href={'/'} target={'_blank'} rel="noreferrer"><FaWhatsappSquare /></a>
          <a href={'/'} target={'_blank'} rel="noreferrer"><AiFillInstagram/></a>
        </span>
      </div>
    </footer>
  )
}
export default Footer;
